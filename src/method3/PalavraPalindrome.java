package method3;

/*
* O objetivo deste exercício é imprimir no console true ou false ao interpretar
* uma String passada por parâmetro como palíndrome ou não.
*
* Autor: Laércio Filho.
*
* */

public class PalavraPalindrome {

    public PalavraPalindrome() {
        imprimirResultado();
    }

    private void imprimirResultado() {
        System.out.println("É palíndrome? " + verificarString("arar"));
    }

    private boolean verificarString(String palavra) {
        return palavra.equals(new StringBuilder(palavra).reverse().toString());
    }

    public static void main(String[] args) {
        new PalavraPalindrome();
    }
}
