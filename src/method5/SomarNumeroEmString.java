package method5;

/*
* O objetivo deste exercício é somar todos os números presentes
* em uma String. Ex: "Tenho 20 maçãs e comprei mais 20" = 20 + 20 = 40.
*
* Autor: Laércio Filho.
*
* */

public class SomarNumeroEmString {
    private int soma = 0;

    public SomarNumeroEmString() {
        String s = "Teste 12 e 2 mais 4";
        String[] words = s.split("\\s+");

        for (int i = 0; i < words.length; i++) {
            verificarNumero(words[i]);
        }
        System.out.println("Soma = " + retornarSoma());
    }

    private int retornarSoma() {
        return this.soma;
    }

    private boolean verificarNumero(String texto) {
        try {
            int i = Integer.parseInt(texto);
            somarNumeroEncontrado(i);
        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;
    }

    private void somarNumeroEncontrado(int numero) {
        this.soma = this.soma + numero;
    }

    public static void main(String[] args) {
        new SomarNumeroEmString();
    }
}
