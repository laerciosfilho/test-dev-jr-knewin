package method2;

/*
* O objetivo deste exercício é ler os números contidos em um array do tipo inteiro,
* analisar a proximidade de cada um dos números com o número 0 e retornar apenas
* o número mais próximo de 0.
*
* Autor: Laércio Filho.
*
* */

import java.util.Arrays;

public class NumeroMaisProximoDe0 {
    public NumeroMaisProximoDe0() {
        int[] listaNumeros = new int[] {5, 10, -5, 12, 25, 35};
        System.out.println("O número mais próximo de 0 é: " + verificarProximidadeNumero(listaNumeros));
    }

    private int verificarProximidadeNumero(int[] lista) {
        int[] listaAuxiliar = lista;
        Arrays.sort(listaAuxiliar); // Ordena lista em ordem crescente.

        int posicaoNumeroNegativo = 0;
        int posicaoNumeroPositivo = 0;
        int numeroMaisProximoDeZero;

        for( int i = 0; i < listaAuxiliar.length; i++) {
            /*
            * Se número da lista na posição i for menor que 0,
            * então incrementa valorNegativo. Senão, incremen-
            * ta valorPositivo.
            *
            * */
            if (listaAuxiliar[i] < 0) {posicaoNumeroNegativo++;
                posicaoNumeroPositivo++;
            }
            /*
            * E se for igual a 0, então incrementa valorPositivo.
            *
            * */
            if (listaAuxiliar[i] == 0) posicaoNumeroPositivo++;
        }
        --posicaoNumeroNegativo; // Garante que os números negativos sejam considerados.

        /*
        * Se número presente na posição 0 da listaAuxiliar for maior ou igual a 0,
        * então o numero mais próximo de 0 é igual ao número da listaAuxiliar
        * presente na posição referente a incrementação do valorPositivo.
        *
        * */
        if (listaAuxiliar[0] >= 0) numeroMaisProximoDeZero = listaAuxiliar[posicaoNumeroPositivo];
        else {
            /*
            * Se número presente na posição referente a incrementação de valorNegativo for
            * menor que o número prensente na posição referente a incrementação de valorPositivo,
            * então o número mais próximo de 0 é o valorNegativo. Senão, será o valorPositivo.
            *
            * listaAuxiliar[posicaoNumeroNegativo]*-1 > Está multiplicação garante que o número
            * positivo, caso haja dois número iguais (-5 e 5), seja priorizado.
            *
            * */
            if ((listaAuxiliar[posicaoNumeroNegativo]*-1) < listaAuxiliar[posicaoNumeroPositivo]) {
                numeroMaisProximoDeZero = (listaAuxiliar[posicaoNumeroNegativo]);
            } else numeroMaisProximoDeZero = listaAuxiliar[posicaoNumeroPositivo]; }
        return numeroMaisProximoDeZero;
    }

    public static void main(String[] args) {
        new NumeroMaisProximoDe0();
    }
}
