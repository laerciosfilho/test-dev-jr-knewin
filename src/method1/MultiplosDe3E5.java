package method1;

/*
* O objetivo deste exercício é imprimir no console a sequência de números de 1 à 100,
* sendo que é necessário substituir os números múltiplos de 3 pelo texto "Fizz", os
* múltiplos de 5 pelo texto "Buzz" e os múltiplos de 3 e 5 pelo texto "FizzBuzz".
*
* Autor: Laércio Filho
*
* */

public class MultiplosDe3E5 {

    public MultiplosDe3E5() {
        retornarMultiplos();
    }

    private void retornarMultiplos() {
        int i = 1;
        while(i < 101) {
            if (i % 3 == 0 && i % 5 == 0) {
                System.out.println("FizzBuzz");
            } else if (i % 5 == 0) {
                System.out.println("Buzz");
            } else if (i % 3 == 0){
                System.out.println("Fizz");
            } else {
                System.out.println(i);
            }
            i++;
        }
    }

    public static void main (String[] args) {
        new MultiplosDe3E5();
    }
}
