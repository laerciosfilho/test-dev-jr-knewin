package method4;

/*
* O objetivo deste exercício é passar por parâmetro uma lista de números inteiros aleatórios,
* podendo ser tanto negativos quanto positivos, e retornar uma nova lista com apenas os números
* positivos contidos na lista passada por parâmetros.
*
* Autor: Laércio Filho.
* */

import java.util.ArrayList;

public class NumerosPositivos {

    public NumerosPositivos() {
        imprimirLista();
    }

    private void imprimirLista() {
        ArrayList<Integer> lista = new ArrayList<>();
        lista.add(0);
        lista.add(-1);
        lista.add(2);
        lista.add(50);

        ArrayList<Integer> novaLista = retornarLista(lista);
        for (int i = 0; i < novaLista.size(); i++) {
            System.out.println(novaLista.get(i).intValue());
        }
    }

    private ArrayList<Integer> retornarLista(ArrayList<Integer> lista) {
        ArrayList<Integer> listaAux = new ArrayList<>();
        for (int i = 0; i < lista.size(); i++) {
            if (lista.get(i).intValue() >= 0) {
                listaAux.add(lista.get(i).intValue());
            }
        }
        return listaAux;
    }

    public static void main(String []args) {
        new NumerosPositivos();
    }
}
